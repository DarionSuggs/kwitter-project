import React, { Component } from "react";
import { Icon } from "semantic-ui-react";

class Likes extends Component {
  render() {
    return (
      <React.Fragment>
        <Icon name="heart" />
      </React.Fragment>
  )}
}

export default Likes;
